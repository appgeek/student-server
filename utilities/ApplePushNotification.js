/**
 * Created by Wael on 11/16/16.
 */

var fs = require('fs')


module.exports = function() {
    return {

        doIt: function(deviceToken, alert, payload, callback) {

            var key

            if (process.env.NODE_ENV == "production") {
                key = "/root/certifications/"
            } else {
                key = "/Users/Wael/Documents/Nodejs/littleServer/certifications/"
            }

            try {
                var apn = require('apn');
                var options = {
                    token: {
                        key: fs.readFileSync(key + 'APNsAuthKey_WY8SHB647H.p8'),
                        keyId: "WY8SHB647H",
                        teamId: "GXL4M9LBQ5"
                    },
                    production: false
                };

                var apnProvider = new apn.Provider(options);

                var note = new apn.Notification();

                note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                note.badge = 1;
                note.sound = "ping.aiff";
                note.alert = alert;
                note.payload = payload;
                note.topic = "com.AppGeekPlus.CodingMoonV3";

                apnProvider.send(note, deviceToken).then(function () {
                    console.log("hello si Wael --> :) ")
                    return callback({ success: true, msg: "Notification work is done :) " });
                });
            } catch (err) {

                console.log(err)
                return callback({ success: false, err: err });

            }


        }



    }
}