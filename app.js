var express = require('express')
var bodyParser = require('body-parser')
var Sequelize = require('sequelize');
var https = require('https')
var http = require('http')
var models = require('./models')
var fs = require('fs')
var server
var port

var options = {
    force: false,
    logging: false
};

models.sequelize.sync(options).then(function () {

    var app = express();


    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

    //path.normalize(__dirname + '/../../'

    //app.use(express.static(path.join(__dirname, 'images')));

    require('./config/routes')(app);

    if (process.env.NODE_ENV == "production") {

        require('ssl-root-cas/latest')
            .inject()
            .addFile('/etc/letsencrypt/live/geektoprofessional.com/privkey.pem')
            .addFile('/etc/letsencrypt/live/geektoprofessional.com/fullchain.pem');

        var sslOptions = {
            key: fs.readFileSync('/etc/letsencrypt/live/geektoprofessional.com/privkey.pem', 'ascii'),
            cert: fs.readFileSync('/etc/letsencrypt/live/geektoprofessional.com/fullchain.pem', 'ascii'),
            passphrase: '1475963'
        };

        server = https.createServer(sslOptions);
        port = 443
    } else {
        server = http.createServer();
        port = 3000
    }

    server.on('request', app);
    server.listen(port, function () {

        console.log('------------------------------------------ The environment is : ', process.env.NODE_ENV);
        console.log("listening on port " + server.address().port);
    });

});
