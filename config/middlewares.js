var models = require('../models')

module.exports = {

    isAuthenticated: function (req, res, next) {
        if (req.headers.token) {

            models.MobileSession.findOne({
                where: {
                    secretKey: req.headers.token
                }
                ,
                include: {
                    model: models.User,
                    attributes: ['id', 'name', 'lastName', 'email', 'region', 'phone', 'type'],
                    include: {
                        model: models.Association,
                        attributes: ['id', 'Description', 'ImgUrl']
                    }
                }
            }).then(function (session) {

                if (session) {

                    req.user = session.User.dataValues

                    console.log(req.user)

                    return next();
                } else {
                    return res.json({success: false, msg: "You have wrong access token"})
                }


            }).catch(function (err) {
                return res.json({success: false, msg: err})
            });


        } else {
            return res.json({success: false, msg: "Please send the token in headers"});
        }
    }

    //,
    //
    //isAssociation: function (req, res, next) {
    //    if (req.user.Association) {
    //        return next();
    //    } else {
    //        return res.json({success: false, msg: "You don't have permission"});
    //    }
    //}



};
