var models = require('../models'),
    tokensRoutes = require('../core/tokens.js')(models),
    authRoutes = require('../core/auth.js')(models)
Campaign = require('../core/Campaign.js')(models)
Volunteers = require('../core/Volunteers.js')(models)
Donates = require('../core/Donates.js')(models)
tokens = require('../core/tokens.js')(models)

module.exports = function(app) {

    // app.post('/api/login', auth.mobileLogin);
    // app.post('/api/register', auth.mobileRegister);
    // app.post('/api/user', auth.mobileUserData);

    app.use('/api/token', tokensRoutes);
    app.use('/api/auth', authRoutes);
    app.use('/api/Campaign', Campaign);
    app.use('/api/Volunteers', Volunteers);
    app.use('/api/Donates', Donates);
    app.use('/api', tokens);

}
