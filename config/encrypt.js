var bcrypt = require('bcrypt-nodejs')

exports.generateHash = function(text) {
    return bcrypt.hashSync(text, bcrypt.genSaltSync(10), null);
};

exports.validHash = function(copy, original) {
    return bcrypt.compareSync(copy, original);
};
