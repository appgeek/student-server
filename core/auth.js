var express = require('express');
var router = express.Router();
var encrypt = require('../config/encrypt')
var crypto = require('crypto');
//var pushNotification = require('../utilities/ApplePushNotification')()

module.exports = function (db) {

    router.post('/register', function (req, res) {

        if (req.body.email && req.body.password) {

            db.User.create({
                email: req.body.email,
                password: encrypt.generateHash(req.body.password),
                name: req.body.name,
                lastName: req.body.lastName,
                region: req.body.region,
                phone: req.body.phone,
                type: req.body.type

            }).then(function (user) {

                db.MobileSession.create({
                    secretKey: crypto.createHash('sha256').update(crypto.randomBytes(30).toString('hex')).digest('hex'),
                    UserId: user.id,
                    deviceToken: req.body.deviceToken
                }).then(function (createdSession) {

                    if (req.body.type == 2) {

                        db.Association.create({
                            Name: req.body.AssociationName,
                            Description: req.body.Description,
                            ImgURL: req.body.ImgURL,
                            UserId: user.id
                        }).then(function () {

                            return res.json({
                                success: true,
                                secretKey: createdSession.secretKey
                            });

                        }).catch(function (err) {
                            return res.json({
                                success: false,
                                msg: err
                            });
                        });

                    } else {

                        return res.json({
                            success: true,
                            secretKey: createdSession.secretKey
                        });
                    }

                }).catch(function (err) {
                    return res.json({
                        success: false,
                        msg: err
                    });
                });

            }).catch(function (err) {

                var msg

                if (err.message == "Validation error") {
                    msg = 'Email has been already taken'
                } else if (err.message == "Validation error: Validation isEmail failed") {
                    msg = 'Incorrect email format'
                } else {
                    msg = 'Sever error'
                }

                console.log()

                return res.json({
                    success: false,
                    msg: msg,
                    err: err
                });

            });
        } else {
            return res.json({
                success: false,
                msg: "L'adresse e-mail et le mot de passe sont obligatoires"
            });
        }

    })

    router.post('/login', function (req, res) {

        if (req.body.email && req.body.password) {

            db.User.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            }).then(function (user) {

                if (user && encrypt.validHash(req.body.password, user.password)) {

                    var newToken = crypto.createHash('sha256').update(crypto.randomBytes(30).toString('hex')).digest('hex')

                    db.MobileSession.update({
                        secretKey: newToken,
                        deviceToken: req.body.deviceToken
                    }, {
                        where: {
                            UserId: user.id
                        }
                    }).then(function () {

                        return res.json({
                            success: true,
                            secretKey: newToken
                        });

                    }).error(function (err) {
                        return res.json({
                            success: false,
                            msg: "L'adresse e-mail ou mot de passe est incorrect",
                            err: err
                        });
                    });

                } else {
                    return res.json({
                        success: false,
                        msg: "L'adresse e-mail ou le mot de passe est incorrect"
                    });
                }
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: "L'adresse e-mail ou le mot de passe est incorrect",
                    err: err
                });
            });

        } else {
            return res.json({
                success: false,
                msg: "L'adresse e-mail et le mot de passe sont obligatoires"
            });
        }

    })

    return router;
};
