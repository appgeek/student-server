/**
 * Created by Wael on 11/19/16.
 */

var express = require('express');
var mdw = require('../config/middlewares.js')
var router = express.Router();
var fs = require("fs")

module.exports = function (db) {


    /////////////      Client request        ////////////


    router.post('/client', mdw.isAuthenticated, function (req, res) {

        db.Campaign.findOne({
            where: {
                id: req.body.CampaignId
            }
        }).then(function (campaign) {

            if (campaign) {

                db.Volunteers.create({

                    UserId: req.user.id,
                    CampaignId: req.body.CampaignId

                }).then(function () {

                    db.Campaign.update({
                        CurrentVolunteers: req.user.id
                    }, {
                        where: {
                            id: campaign.id
                        }
                    }).then(function () {

                        res.json({
                            success: true,
                            msg: "All Done"
                        });

                    }).catch(function (err) {
                        res.json({
                            success: false,
                            msg: err
                        });
                    });


                }).catch(function (err) {
                    res.json({
                        success: false,
                        msg: err
                    });
                });

            } else {

                res.json({
                    success: false,
                    err: "Wrong CampaignId"
                });
            }

        }).catch(function (err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })


    router.get('/client', mdw.isAuthenticated, function (req, res) {

        db.Volunteers.findAll({
            where: {
                UserId: req.user.id
            }, include: {
                model: db.Campaign
            }
        }).then(function (Volunteers) {

            res.json({
                success: true,
                Volunteers: Volunteers
            });

        }).catch(function (err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })

    return router;
};