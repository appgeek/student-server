/**
 * Created by Wael on 11/19/16.
 */

var express = require('express');
var mdw = require('../config/middlewares.js')
var router = express.Router();
var fs = require("fs")

module.exports = function(db) {

    /////////////      Admin request        ////////////

    router.post('/admin/', mdw.isAuthenticated, /*mdw.isAssociation, */function(req, res) {

        //var binaryData = new Buffer(req.body.image, 'base64').toString('binary');
        //
        //fs.writeFile("./out.png", base64Data, 'base64', function(err) {
        //    return res.json({
        //        success: false,
        //        msg: err
        //    });
        //});

        db.Campaign.create({

            Title: req.body.Title,
            Description: req.body.Description,
            ShortDescription: req.body.ShortDescription,
            ImgUrl: "bara zamer gilni",  // ------------------------
            CurrentDonates: 0,
            CurrentVolunteers: 0,
            RequiredDonates: req.body.RequiredDonates,
            RequiredVolunteers: req.body.RequiredVolunteers,
            Region: req.body.Region,
            DeadLine: req.body.DeadLine,
            Latitude: req.body.Latitude,
            Longitude: req.body.Longitude,
            AssociationId :  req.user.Associations[0].dataValues.id

        }).then(function(campaign) {

            res.json({
                success: true,
                campaign: campaign
            });

        }).catch(function(err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })



    router.get('/admin/', mdw.isAuthenticated, /*mdw.isAssociation, */function(req, res) {

        db.Campaign.findAll({

            where : {
                AssociationId : req.user.Association.id
            }

        }).then(function(campaign) {

            res.json({
                success: true,
                campaign: campaign
            });
        }).catch(function(err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })


    /////////////      Client request        ////////////

    router.get('/client/', mdw.isAuthenticated, function(req, res) {

        //var binaryData = new Buffer(req.body.image, 'base64').toString('binary');
        //
        //fs.writeFile("./out.png", base64Data, 'base64', function(err) {
        //    return res.json({
        //        success: false,
        //        msg: err
        //    });
        //});

        db.Campaign.findAll({

            where: {
                DeadLine : {
                    $gt : new Date()
                }
            },include: {
                model: db.Association,
                attributes: ['Name', 'Description', 'ImgUrl']
            }

        }).then(function(campaigns) {

            res.json({
                success: true,
                campaigns: campaigns
            });

        }).catch(function(err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })

    return router;
};