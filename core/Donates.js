/**
 * Created by Wael on 11/19/16.
 */

/**
 * Created by Wael on 11/19/16.
 */

var express = require('express');
var mdw = require('../config/middlewares.js')
var router = express.Router();

module.exports = function (db) {


    /////////////      Client request        ////////////


    router.post('/client', mdw.isAuthenticated, function (req, res) {

        db.Donates.create({

            Code: req.body.Code,
            Amount: req.body.Amount,
            State: 1,
            UserId: req.user.id,
            CampaignId: req.body.CampaignId

        }).then(function () {

            res.json({
                success: true,
                msg: "All done"
            });

        }).catch(function (err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })


    router.get('/admin/', mdw.isAuthenticated, function (req, res) {

        db.Donates.findAll({

            where : {
                state : 1
            },include: {
                model: db.Campaign
                //,include: {
                //    model: db.Association , where: {
                //        UserId : req.user.id
                //    }
                //}
            }

        }).then(function (donates) {

            res.json({
                success: true,
                donates: donates
            });

        }).catch(function (err) {
            res.json({
                success: false,
                msg: err
            });
        });

    })


    router.post('/admin/', mdw.isAuthenticated, function (req, res) {

        if (req.body.state == 2 || req.body.state == 3) {


            db.Donates.findOne({

                where: req.body.DonateId

            }).then(function (donate) {

                console.log("-----------> here boss !!!")


                db.Donates.update({

                    state: req.body.state

                },{
                    where : {
                        id : req.body.DonateId
                    }
                }).then(function () {

                    if (req.body.state == 2) {

                        db.Campaign.update({

                            CurrentDonates: donate.Amount

                        },{
                            where : {
                                id: donate.CampaignId
                            }
                        }).then(function () {

                            res.json({
                                success: true,
                                msg: "All done"
                            });

                        }).catch(function (err) {
                            res.json({
                                success: false,
                                msg: err
                            });
                        });

                    } else {
                        res.json({
                            success: true,
                            msg: "All done"
                        });
                    }

                }).catch(function (err) {
                    res.json({
                        success: false,
                        msg: err
                    });
                });

            }).catch(function (err) {
                res.json({
                    success: false,
                    msg: err
                });
            });


        } else {
            res.json({
                success: false,
                msg: "wrong state params"
            });
        }

    })

    return router;
};