'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return     queryInterface.addColumn('Campaigns', 'ShortDescription', Sequelize.STRING);
  },

  down: function (queryInterface) {
    return     queryInterface.removeColumn('Campaigns', 'ShortDescription');

  }
};
