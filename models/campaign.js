/**
 * Created by Wael on 11/18/16.
 */

'use strict';

module.exports = function (sequelize, DataTypes) {

    var Campaign = sequelize.define('Campaign', {

        Title: DataTypes.STRING,
        Description: DataTypes.STRING,
        ShortDescription: DataTypes.STRING,
        ImgUrl: DataTypes.STRING,
        CurrentDonates: DataTypes.FLOAT,
        CurrentVolunteers: DataTypes.INTEGER,
        RequiredDonates: DataTypes.FLOAT,
        RequiredVolunteers: DataTypes.INTEGER,
        Region: DataTypes.STRING,
        DeadLine: DataTypes.DATE,
        Latitude: DataTypes.FLOAT,
        Longitude: DataTypes.FLOAT

    }, {
        classMethods: {
            associate: function (models) {
                Campaign.belongsTo(models.Association);

                Campaign.hasMany(models.Volunteers);
                Campaign.hasMany(models.Donates);

            }
        }
    });

    return Campaign;
};
