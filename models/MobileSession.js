'use strict';
module.exports = function (sequelize, DataTypes) {
    var MobileSession = sequelize.define('MobileSession', {
        secretKey : {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        deviceToken: DataTypes.STRING
    },{
        classMethods: {
            associate: function(models) {
                MobileSession.belongsTo(models.User);
            }
        }
    });

    return MobileSession;
};
