/**
 * Created by Wael on 11/18/16.
 */

'use strict';

module.exports = function (sequelize, DataTypes) {

    var Donates = sequelize.define('Donates', {

        Code: DataTypes.STRING,
        Amount: DataTypes.FLOAT,
        State: DataTypes.INTEGER  // 1 pending , 2 approved , 3 ignored

    }, {
        classMethods: {
            associate: function (models) {
                Donates.belongsTo(models.Campaign);
                Donates.belongsTo(models.User);
            }
        }
    });

    return Donates;
};
