/**
 * Created by Wael on 11/18/16.
 */

'use strict';

module.exports = function (sequelize, DataTypes) {

    var Event = sequelize.define('Event', {

        Title: DataTypes.STRING,
        ShortDescription: DataTypes.STRING,
        LongDescription: DataTypes.TEXT,
        FinancialRapport: DataTypes.TEXT,
        ImgUrl: DataTypes.STRING

    }, {
        classMethods: {
            associate: function (models) {
                Event.belongsTo(models.Association);
                Event.hasMany(models.Feedback);
            }
        }
    });

    return Event;
};
