/**
 * Created by Wael on 11/18/16.
 */

'use strict';

module.exports = function (sequelize, DataTypes) {

    var Volunteers = sequelize.define('Volunteers', {


    }, {
        classMethods: {
            associate: function (models) {
                Volunteers.belongsTo(models.Campaign);
                Volunteers.belongsTo(models.User);
            }
        }
    });

    return Volunteers;
};

