
'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
            validate: {
                notEmpty: true,
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING,
            validate: {
                // len: [6, 30],
                notEmpty: true
            }
        },
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                len: {
                    args: [3, 15],
                    msg: "Le longueur de Nom doit être comprise entre 3 et 15"
                },
            }
        },
        lastName: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                len: {
                    args: [3, 15],
                    msg: "Le longueur de Prénom doit être comprise entre 3 et 15"
                },
            }
        },
        region: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING,
            validate: {
                len: {
                    args: [8, 8],
                    msg: "N° Téléphone doit être 8 numéros"
                }
            }
        },
        type: {
            type: DataTypes.INTEGER // 1 normal user, 2 association
        }
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Association);
                User.hasMany(models.Volunteers);
                User.hasMany(models.Donates);
                User.hasMany(models.Feedback);
            }
        }
    });

    return User;
};
