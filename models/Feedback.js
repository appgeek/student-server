/**
 * Created by Wael on 11/18/16.
 */

'use strict';

module.exports = function (sequelize, DataTypes) {

    var Feedback = sequelize.define('Feedback', {

        Rating: DataTypes.INTEGER,
        Comment: DataTypes.TEXT

    }, {
        classMethods: {
            associate: function (models) {
                Feedback.belongsTo(models.User);
                Feedback.belongsTo(models.Event);
            }
        }
    });

    return Feedback;
};