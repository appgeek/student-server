'use strict';

module.exports = function (sequelize, DataTypes) {

    var Association = sequelize.define('Association', {
        Name: DataTypes.STRING,
        Description: DataTypes.STRING,
        ImgUrl: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                Association.belongsTo(models.User);

                Association.hasMany(models.Campaign);
                Association.hasMany(models.Event);

            }
        }
    });

    return Association;
};
